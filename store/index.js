export const state = () => ({
  notifications: []
});

export const mutations = {
  addNotification(state, title, text) {
    state.notifications.push( {
      group : 'foo',
      title : title,
      text : text,
      id: state.notifications.length+1
    })
  },
  delNotification(state,notif) {
    state.notifications.splice(state.notifications.indexOf(notif),1);
  }
}
