FROM node:lts-alpine
WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/
RUN npm install
RUN npm run build
COPY . /usr/src/app

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=80

ENV HOST 0.0.0.0
ENV PORT 80
EXPOSE $PORT
CMD [ "nuxt", "start" ]
