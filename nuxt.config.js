export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr:false,
  publicRuntimeConfig: {
    baseURL: "http://14-bit.com:8888/",

  },
  head: {
    title: 'BitUp14-front',

    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  server: {
    host : '0.0.0.0',
    port : 80
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/fonts.css',
    'hooper/dist/hooper.css'

  ],
  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        path: '/users/:id/verify',
        components: {
          default: resolve(__dirname, 'pages/users/verify.vue'), // or routes[index].component
        },
      })
    }
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/vue2-editor", ssr: false },
    { src: "~/plugins/v-modal", ssr: false },
    { src: "~/plugins/v-calendar", ssr: false },
    { src: "~/plugins/v-slick-carousel", ssr: false },
    { src: "~/plugins/v-popup", ssr: false },
    { src: "~/plugins/v-screen", ssr: false },

  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    'nuxt-vue-select',
    '@nuxtjs/axios',
    ['cookie-universal-nuxt', { alias: 'cookiz' }],
    '@nuxt/image',
    'nuxt-socket-io',
  ],
  io: {
    // module options
    sockets: [{
      name: 'main',
      url: 'http://14-bit.com:3000'
    }]
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: 'http://14-bit.com:3000/',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}

